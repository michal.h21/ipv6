\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cznic}[2018/10/11 Trida pro knihy CZ.NIC]

\RequirePackage{ifthen}
\newboolean{ebook}\setboolean{ebook}{false}
\newboolean{mobi}\setboolean{mobi}{false}
\newboolean{epub}\setboolean{epub}{false}
\newboolean{web}\setboolean{web}{true}
\newboolean{tisk}\setboolean{tisk}{false}
\DeclareOption{mobi}{
    \setboolean{ebook}{true}
    \setboolean{mobi}{true}
    \setboolean{web}{false}}
\DeclareOption{epub}{
    \setboolean{ebook}{true}
    \setboolean{epub}{true}
    \setboolean{web}{false}}
\DeclareOption{web}{
    \setboolean{web}{true}}
\DeclareOption{tisk}{
    \setboolean{tisk}{true}
    \setboolean{web}{false}}

\ProcessOptions

\LoadClass[b5paper,twoside,openright,10pt]{book}
\RequirePackage{tabularx}
\RequirePackage{graphicx}
\RequirePackage{rotating}

\RequirePackage[table,cmyk]{xcolor}
\definecolor{UltraLight}{gray}{0.9}
\definecolor{VeryLight}{gray}{0.8}
\definecolor{Light}{gray}{0.8}
\definecolor{Dark}{gray}{0.5}
\definecolor{TabHeader}{gray}{1}
\definecolor{PozadiStranky}{gray}{0.8}
\definecolor{Podtrzeni}{gray}{0.6}
\definecolor{Odkazy}{gray}{0}

\RequirePackage{makeidx}
\makeindex

\RequirePackage{hyperref}

\ifthenelse{\boolean{web} \OR \boolean{tisk}}{
\hypersetup{
    linktoc=all,
    bookmarksnumbered=true,
    colorlinks=true,
    linkcolor=Odkazy,
    urlcolor=Odkazy,
    citecolor=Odkazy}
\input{cznic-xelatex}}{
\input{cznic-htlatex}}

%
% předěly a nadpisy
%
\newcommand{\cast}[2]{\cleardoublepage\makeatletter\@openrightfalse
\part{#1}
\input{#2}
\@openrighttrue\makeatother}

\newenvironment{uvodCasti}{\pagecolor{PozadiStranky}\mbox{}\vfil
\thispagestyle{empty}}%
{\vfil\mbox{}\newpage\pagecolor{white}}

\newenvironment{predelNadpis}%
{\cleardoublepage\bgroup\pagecolor{PozadiStranky}\thispagestyle{empty}\mbox{}\vfill
    \sffamily\bfseries\fontsize{30}{36}\selectfont}%
{\vfill\vfill\vfill\mbox{}\egroup\clearpage\pagecolor{white}}

\newcommand{\predelPrazdna}{\pagecolor{PozadiStranky}\prazdnaStrana\pagecolor{white}}

\newcommand{\prazdnaStrana}{\mbox{}\thispagestyle{empty}\vfill\mbox{}\clearpage}

\newcommand{\predel}[1]{\begin{predelNadpis}
#1
\end{predelNadpis}
\predelPrazdna}

\newcommand{\malyNadpis}[2][]{\clearpage
\phantomsection
\addcontentsline{toc}{section}{#2}
\ifthenelse{ \equal{#1}{} }
    {\section*{#2}}
    {\section*{#1}}
\paticka{#2}}

%
% barvy
%
% šedé pozadí
\newcommand{\sede}[1]{\colorbox{Light}{#1}}

% tabulky
\newcommand{\colortable}{\rowcolors{1}{VeryLight}{UltraLight}\def\arraystretch{1.5}%
\setboolean{odkobrysy}{false}}
\newcommand{\tabheader}{\rowcolor{Dark}\color{TabHeader}}
\newcommand{\tabsubheader}[2]{\rowcolor{Dark}\multicolumn{#1}{l}{\color{TabHeader}\textbf{#2}}}
\newcommand{\hlavickapismo}[1]{\textbf{#1}}
\newcommand{\hlavicka}[1]{\hlavickapismo{\color{TabHeader}#1}}

% pokračování tabulek a obrázků
\newenvironment{contfigure}{%
\addtocounter{figure}{-1}%
\begin{figure}[htp]%
\renewcommand{\thefigure}{\arabic{chapter}.\arabic{figure} (pokračování)}}{%
\end{figure}}
\newenvironment{conttable}{%
\renewcommand{\thetable}{\arabic{chapter}.\arabic{table} (pokračování)}%
\addtocounter{table}{-1}%
\begin{table}[htp]}{%
\renewcommand{\thetable}{\arabic{chapter}.\arabic{table}}%
\end{table}}
%
% podtrhávání odkazů
%
\RequirePackage[normalem]{ulem}
\renewcommand{\ULthickness}{0.5pt}
\renewcommand{\ULdepth}{1.8pt}
\RequirePackage{contour}
\contourlength{1.0pt}
\newboolean{odkpodtrh}\setboolean{odkpodtrh}{false}
\newboolean{odkobrysy}\setboolean{odkobrysy}{true}
\newcommand\grayuline{\bgroup\markoverwith{\textcolor{Podtrzeni}{\rule[-0.5ex]{2pt}{0.4pt}}}\ULon}

%\newcommand{\podtrhnout}[1]{#1}
\newcommand{\podtrhnout}[1]{\ifthenelse{\boolean{odkpodtrh}}{\grayuline{\phantom{\smash{#1}}}%
\llap{\ifthenelse{\boolean{odkobrysy}}{\contour{white}{#1}}{\llap{#1}}}}{#1}}
\newcommand{\odkazyPodtrhnout}{\setboolean{odkpodtrh}{true}}
\newcommand{\odkazyNepodtrhavat}{\setboolean{odkpodtrh}{false}}
%
% písma
%
\newcommand{\stdvstup}[1]{\textit{\ttfamily #1}}
\newcommand{\malyvstup}[1]{\textit{\small\ttfamily #1}}
\newcommand{\vstup}[1]{\stdvstup{#1}}
%
% obrázky
%
\newcommand{\obrazek}[1]{\includegraphics{img/#1}}
\newcommand{\veleobr}[1]{\makebox[\textwidth]{\hskip 0pt plus 1fill
 minus 1 fill\mbox{\obrazek{#1}}}}
\newcommand{\doleva}[1]{\makebox[\textwidth]{\hskip 0pt plus 1fill
 minus 1 fill\mbox{#1}}}
%
% klávesa, např. \key{Q}
%
\newcommand{\key}[1]{{\fboxsep=2pt\fcolorbox{Dark}{Light}{\footnotesize\textsf{\bfseries\upshape #1}}}}
%
% dvojklávesy a klávesa s významem (např. \comkey{F1}{Help})
%
\newcommand{\ctrl}[1]{\key{Ctrl-#1}}
\newcommand{\alt}[1]{\key{Alt-#1}}
\newcommand{\shift}[1]{\key{Shift-#1}}
\newcommand{\ctralt}[1]{\key{Ctrl-Alt-#1}}
\newcommand{\comkey}[2]{\mbox{\sf\key{#1}\kern0.2em-#2}}
%
% upozornění na pojmy - na okraj textu
% důležité pojmy - sázeny kurzívou, zároveň zařazeny do indexu a uvedeny
%  na okraji
%\ifebook
    \newcommand{\naokraj}[1]{}
%\else
%    \newcommand{\naokraj}[1]{\pagebreak[3]\mbox{}\marginpar[\hspace{0pt}\raggedleft\bfseries
%                                        #1]{\hspace{0pt}\raggedright\bfseries #1}}
%    \newcommand{\naokraj}[1]{\textbf{\sffamily #1:}\enspace}
%\fi
\newcommand{\pojem}[1]{\naokraj{#1}\hlindex{#1}\emph{#1}}
%
% viditelná mezera
%\newcommand{\mezera}{{\tt\char32}}
\newcommand\mezera[1][.35em]{\mbox{\kern.06em\rule{0.05em}{.4ex}\rule{#1}{0.05em}\rule{0.05em}{.4ex}}}
%
% jména souborů
\newcommand{\soubor}[1]{\emph{#1}}
%
% prvky GUI
\newcommand{\menu}[1]{\textbf{#1}}
\newcommand{\tlac}[1]{\menu{#1}}
\newcommand{\dalsi}{\,/\,}
%
% jména programů
\newcommand{\program}[1]{\textit{#1}}
%
% odkazy v dokumentu
\newcommand{\refP}[1]{\podtrhnout{\ref{#1}}}
\newcommand{\pagerefP}[1]{\podtrhnout{\pageref{#1}}}
\newcommand{\kap}[1]{kapitola~\refP{#1} na straně~\pagerefP{#1}}
\newcommand{\odkaz}[1]{\refP{#1} na straně~\pagerefP{#1}}
%
% webové odkazy
\newcommand{\webadresa}[1]{\href{#1}{\podtrhnout{#1}}}
\newcommand{\webodkaz}[2]{\href{#1}{\podtrhnout{#2}}}
\newcommand{\naweb}[1]{\ikonalink\webadresa{#1}}
\newcommand{\nawebjinak}[2]{\ikonalink\webodkaz{#1}{#2}}
%
% prostředí pro příklady
\newcommand{\startpr@stredi}[1]{\subsubsection*{#1}}
\newcommand{\stoppr@stredi}{\rule{1ex}{1ex}}
\newcommand{\couvni}{\vspace*{-1.5\baselineskip}}
\newdimen\sirkamezery
\setbox0=\hbox{ }\sirkamezery=\wd0
\newenvironment{priklad}{\startpr@stredi{Příklad:}}{\stoppr@stredi}
%
% posunutý text
\newenvironment{posun}[1]
   {\list{}{\leftmargin #1}\item[]}
   {\endlist}
%
% prostředí pro ukázky počítačových výstupů
\newenvironment{ukazka}{\begin{quote}\obeyspaces\tt\parskip=10pt}{\end{quote}}
%
% odsazení od levého okraje
\newenvironment{leveodsazeni}{\begin{itemize}\item[]}{\end{itemize}}
%
% prostředí pro definice příkazů
\newenvironment{prikaz}{\par\leftskip=-\leftmarginwidth \bf}{\par}
%
% \hlavnindex je příkaz, kterým se sází odkazy na stránky s definicí
% \hlindex je alternativa \index pro odkaz na definice
% je-li definice vícestránková, vymezují ji \histart a \histop
% obdobně pro obyčejné indexy \istart a \istop
% \vizindex je nepřímý odkaz do rejstříku (bude obsahovat "#1 viz #2")
%
\newcommand{\hlavnindex}[1]{\textbf{#1}}
%\newcommand{\hlindex}[1]{\index{#1|hlavnindex}}
\newcommand{\hlindex}[1]{\index{#1}}
\newcommand{\histart}[1]{\index{#1|(}}
\newcommand{\histop}[1]{\index{#1|)}}
\newcommand{\istart}[1]{\index{#1|(}}
\newcommand{\istop}[1]{\index{#1|)}}
\newcommand{\vizindex}[2]{\index{#1|see{#2}}}

% vlastní definice kapitoly
\newcommand{\kapitola}[1]{\chapter{#1}}
\newcommand{\paticka}[1]{\markboth{#1}{}}

\def\appendixname{}

%
% prodloužení a zkrácení aktuální stránky
%
\newcommand{\dlouhastr}{\enlargethispage{\baselineskip}}
\newcommand{\kratkastr}{\enlargethispage{-\baselineskip}}

\unitlength=1mm
\raggedbottom
\frenchspacing
\message{\string\frenchspacing\space is set on.}

%\setcounter{page}{3}
\setcounter{secnumdepth}{2}
\setcounter{tocdepth}{2}
\sloppy

%
% definice pro specifická média
%
\ifthenelse{\boolean{tisk}}{\input{cznic-tisk}}{}

%
% pro tiráž
%
\newcommand{\cznicvydavatel}{Vydavatel:\\
CZ.NIC, z.\,s.\,p.\,o.\\
Milešovská 5, 130 00 Praha 3\\
Edice CZ.NIC\\
\webodkaz{https://www.nic.cz/}{www.nic.cz}}

\newcommand{\czniclicence}{Toto autorské dílo podléhá licenci Creative Commons BY-ND 3.0\\
(\webadresa{http://creativecommons.org/licenses/by-nd/3.0/cz/}),\\
jeho sdílení je tedy možné za předpokladu, že zůstane zachováno označení autora
díla a prvního vydavatele díla, sdružení CZ.NIC, z.\,s.\,p.\,o. Dílo může být
překládáno a následně šířeno v písemné či elektronické formě na území
kteréhokoliv státu.}

\newcommand{\czniclogo}{\includegraphics[width=3.5cm]{img/CZ-NIC-logo.eps}}

\newcommand{\isbn}{\ifthenelse{\boolean{web}}{\isbnweb}{%
\ifthenelse{\boolean{tisk}}{\isbntisk}{%
\ifthenelse{\boolean{mobi}}{\isbnmobi}{\isbnepub}}}}

\setcounter{errorcontextlines}{15}

\fboxsep=2pt

\widowpenalty=10000
\clubpenalty=10000
\raggedbottom
\setcounter{topnumber}{2}
\setcounter{bottomnumber}{2}
\setcounter{totalnumber}{4}
\renewcommand{\topfraction}{0.85}
\renewcommand{\bottomfraction}{0.85}
\renewcommand{\textfraction}{0.15}
\renewcommand{\floatpagefraction}{0.7}
\setlength{\floatsep}{20pt plus 2pt minus 4pt}

\providecommand\phantomsection{}

\newenvironment{tiraz}{}{}
\endinput

