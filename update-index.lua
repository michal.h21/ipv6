-- update index with links back to html file
--
local idxfile = arg[1]

local pos = 0
local entries = {}
-- find file locations from the .idx file
for line in io.lines(idxfile) do
  --\beforeentry{ipv6.html}{dx1-48015}{}
  -- find file and destination for the next index entry
  local file, dest = line:match("\\beforeentry{(.-)}{(.-)}")
  if file then
    pos = pos+1
    entries[pos] = {file=file, dest = dest}
  end
end

-- read contents of the .ind file from the stdinput
for  line in io.lines() do
  local line = line:gsub("(%s*\\%a+.-%,)(.+)$", function(start,rest)
    return start .. rest:gsub("(%d+)", function(page)
      local entry = entries[tonumber(page)]
      if entry then
        -- construct link to the index entry
        return "\\LNK{" .. entry.file .."}{".. entry.dest .."}{}{" .. page .."}"
      else
        return page
      end

    end)
  end)
  print(line)
end
