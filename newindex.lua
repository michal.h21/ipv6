kpse.set_program_name("luatex")
local index = require "make4ht-indexing"

index.load_enc()
local f = io.open(arg[1], "r")
local content = f:read("*all")
local idx = index.parse_idx(content)
local idxname = os.tmpname()
local indname = os.tmpname()
local f = io.open(idxname, "w")
f:write(idx.idx)
f:close()
local cmd = "texindy -L czech -C utf8 -o ".. indname .. " " .. idxname
os.execute(cmd)
local f = io.open(indname,  "r")
local content = f:read("*all")
f:close()

local newcontent = index.fix_idx_pages(content, idx)
local f = io.open(arg[1]:gsub("idx$","ind"),"w")
f:write(newcontent)
f:close()

os.remove(idxname)
os.remove(indname)



