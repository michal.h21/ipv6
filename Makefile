tex_content = $(wildcard *.tex )
fourht = $(wildcard *.4ht)
mode=draftx

all: ipv6-epub.epub ipv6-mobi.mobi

ipv6-epub.epub: ipv6-epub.tex satrapa.cfg  ${tex_content} cznic.cls ${fourth} build.mk4 cznic.4ht
	tex4ebook -c satrapa.cfg -e build.mk4 -m ${mode} -f epub+common_domfilters -c satrapa.cfg $< "fn-in"

ipv6-mobi.mobi: ipv6-mobi.tex satrapa.cfg  ${tex_content} cznic.cls ${fourth} cznic.4ht 
	tex4ebook -c satrapa.cfg -e build.mk4 -m ${mode} -f mobi+common_domfilters -c satrapa.cfg $< "fn-in"
