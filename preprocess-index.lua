local current_entry = 0
for line in io.lines() do
  if line:match("^\\beforeentry") then
    -- increment index entry number
    current_entry = current_entry + 1
  elseif line:match("^\\indexentry") then
    -- replace the page number with the current
    -- index entry number
    local result = line:gsub("{[0-9]+}", "{"..current_entry .."}")
    print(result)
  else
    print(line)
  end
end
